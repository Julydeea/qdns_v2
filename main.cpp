#include <iostream>
#include <chrono>
#include <thread>
#include <typeinfo>
#include <ldns/ldns.h>

#include "DNS_QueryManager.h"

using namespace std;
using namespace std::chrono;

int main(int argc, char *argv[])
{
	uint32_t query_period;
	if(argc != 2)
	{
		cout << "Please provide query frequency: " << endl;
		cin >> query_period;
	}
	else
	{
		query_period = atoi(argv[1]);
	}

	int count = 0;
	DNS_QueryManager qm(query_period);
	if(!qm.DoDNSQueries())
	{
		return 0;
	}

	return 1;
}
