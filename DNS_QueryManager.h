#ifndef LDNS_QUERYMANAGER_H
#define LDNS_QUERYMANAGER_H

#include<vector>
#include<string>
#include<map>
#include<mutex>
#include<pthread.h>
#include<mysql++.h>
#include<set>
extern "C" 
{ 
#include"thpool.h" 
}

struct query_data {
	uint32_t latency;
	std::string timestamp;

	query_data(): latency(0), timestamp("") { }
};

class DNS_QueryManager 
{
	public:
		DNS_QueryManager(uint32_t);
		~DNS_QueryManager();
		DNS_QueryManager(const DNS_QueryManager &) = delete; 
		DNS_QueryManager operator=(const DNS_QueryManager&) = delete;

		int DoDNSQueries();
		ldns_resolver* GetResolver() { return resolver_; }

		static const std::vector<std::string> domains;
		static void query_thread(void*);

	private:
		int CreateResolver();
		int QueryForDomain(const std::string &);
		int SaveDNSQuery(const std::string &, const uint32_t, const std::string &);
		int UpdateStatistics(const std::string & , const uint32_t, const std::string & );
		int Average(const mysqlpp::Row &, double &, const uint32_t);
		int StandardDeviation(const mysqlpp::Row &, double &, const uint32_t); 
		ldns_resolver *resolver_;
		std::map<std::string, struct query_data> collector_; 
		std::mutex mutex_;
		uint32_t period_;
		threadpool thread_pool_;
};





#endif


