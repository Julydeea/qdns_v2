#include<iostream>
#include<string>
#include<mysql++.h>
#include<fstream>

#include "SQL_Manager.h"

using namespace std;
using namespace mysqlpp;

SQL_Manager::SQL_Manager(Connection & conn) : conn_(conn)
{
	db_name_ = "performance";
	server_ = "192.168.0.104";
	user_ = "testdb";
	pass_file_ = "pass";
}

int SQL_Manager::GetPass()
{
	ifstream file(pass_file_);

	if(file.is_open())
	{
		getline(file, pass_);
		file.close();
	}
	else
	{
		cout << "Error reading password from file " << pass_file_ << endl;
		return 0;
	}

	return 1;
}

int SQL_Manager::CreateConnection()
{
	if(!GetPass())
	{
		cout << "Error getting database password" << endl;
		return 0;
	}

	if(conn_.connect(db_name_.c_str(), server_.c_str(), user_.c_str(), pass_.c_str()))
	{	
		//cout << "Connected to db" << endl;
		return 1;
	}
	else
	{
		cerr << "Failed to connect to database " << db_name_ << endl; 	
		return 0;
	}		
}


int SQL_Manager::DBWrite(const std::string &sql)
{	
	try 
	{
		Query query = conn_.query();
		query << sql.c_str();   

		query.execute();

	}
	catch (const BadQuery& er) {
		cout << "Query error: " << er.what() << endl;
		return 0;
	}    
	catch (const Exception& er) {
		cout << "Error: " << er.what() << endl;
		return 0;
	}

	return 1;
}


int SQL_Manager::ReadStatisticsRow(const string & site_name, Row & row)
{
	string sql = "select * from statistics where site_name='" + site_name +"'";
	try
	{
		Query query = conn_.query();	
		query << sql.c_str();
		if(StoreQueryResult res = query.store())
		{ 
			if(res.num_rows())
				row = *res.begin();
		}
		else
		{
			cout << "Error reading record with site_name " << site_name << endl;
			return 0;
		}
	}
	catch (const BadQuery& er) {
		cout << "Query error: " << er.what() << endl;
		return 0;
	}
	catch (const Exception& er) {
		cout << "Error: " << er.what() << endl;
		return 0;
	}
	return 1;	
}






