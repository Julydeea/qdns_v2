#ifndef DNS_REQUEST_H
#define DNS_REQUEST_H

#include<iostream>
#include<string>

class DNS_Request
{
	public: 
		DNS_Request() {} 
		DNS_Request(const std::string & ); 
		~DNS_Request();
		DNS_Request(const DNS_Request &) = delete;
		DNS_Request& operator=(const DNS_Request&) = delete;

		int SendDNSQuery(ldns_resolver*);
		int GetLatency();
		std::string GetQueryTimestamp();
		const unsigned int maxRand = 1000000;

	private:
		void GenerateRandom();
		std::string BuildDomainName();
		int CreateDomain(const std::string & );

		ldns_pkt* packet_;
		ldns_rdf *domain_;
		uint32_t latency_;
		std::string domain_name_;
		std::string rand_;

};


#endif
