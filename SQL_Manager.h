#ifndef AND_SQL_MANAGER
#define AND_SQL_MANAGER

#include<string>
#include<mysql++.h>

class SQL_Manager 
{

	public:
		SQL_Manager() {}
		SQL_Manager(mysqlpp::Connection & conn);
		~SQL_Manager() {} 
		SQL_Manager(const SQL_Manager& ) = delete;
		SQL_Manager operator=(const SQL_Manager&) = delete;

		int DBWrite(const std::string &);
		int ReadStatisticsRow(const std::string &, mysqlpp::Row &);
		int CreateConnection();
		void SetPassFile(const std::string & filename) { pass_file_ = filename; }

	private:

		std::string db_name_;
		std::string server_;
		std::string user_;
		std::string pass_file_;
		std::string pass_;
		mysqlpp::Connection conn_;
		int GetPass();
};

#endif
