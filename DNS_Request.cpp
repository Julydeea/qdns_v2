#include<iostream>
#include<vector>
#include<random>
#include<ctime>
#include<ldns/ldns.h>
#include "DNS_Request.h"

using namespace std;

DNS_Request::DNS_Request(const string & domain_name): domain_name_(domain_name)
{
	packet_ = NULL;
	domain_ = NULL;
	latency_ = 65536;
	rand_ = "";
}

DNS_Request::~DNS_Request() 
{
	if(domain_)
	{
		ldns_rdf_free(domain_);
		domain_ = NULL;
	}
	if(packet_)
	{
		ldns_pkt_free(packet_);
		packet_ = NULL;
	}	
}

void DNS_Request::GenerateRandom()
{
	random_device random_seed;
	mt19937 generator(random_seed());
	uniform_int_distribution<> dist(1000, maxRand);

	rand_ = to_string(dist(generator));
}

string DNS_Request::BuildDomainName()
{
	if(!domain_name_.empty() && !rand_.empty())
	{
		return rand_ + '.' + domain_name_;
	}
	else 
	{
		cout << "Error building domain name from random string " << rand_ 
			<<  " and domain name " << domain_name_ << endl;
		return rand_;
	}
}

int DNS_Request::CreateDomain(const string & rd_name)
{
	if(!rd_name.empty()) 
	{
		domain_ = ldns_dname_new_frm_str(rd_name.c_str());
		if(!domain_)
		{
			cout << "Error creating domain from domain_name " << rd_name << endl;; 
			return 0;			
		}
		ldns_dname_absolute(domain_);
	}
	else
	{
		cout << "Error creating domain. Domain name empty." << endl;
		return 0;
	}
	return 1;	
}

int DNS_Request::SendDNSQuery(ldns_resolver* resolver)
{
	GenerateRandom();
	string rd_name = BuildDomainName();
	int status = CreateDomain(rd_name);

	if (!status || !resolver)
	{
		cout << "Error sending DNS Query. Invalid domain." << endl;
		return 0;
	}

	packet_ = ldns_resolver_search( resolver,
			domain_,
			LDNS_RR_TYPE_A,
			LDNS_RR_CLASS_IN,
			LDNS_RD);
	if(!packet_)
	{	
		cout << "Error in dns packet" << endl;
		return 0;
	}
	return 1;
}

int DNS_Request::GetLatency()
{
	return packet_->_querytime;
}

string DNS_Request::GetQueryTimestamp()
{	
	time_t time = packet_->timestamp.tv_sec;
	char buff[20] = {'\0'};
	strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&time));

	return string(buff);
}


















